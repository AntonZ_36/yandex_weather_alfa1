/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Yandex_Weather;

import com.mashape.unirest.http.exceptions.UnirestException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.json.JSONObject;

/**
 *
 * @author tonza
 */
public class YandexWeatherTest {
    
    YandexWeather yandexAnswer;
    private String weatherResult, lat, lon;
    
    
    public YandexWeatherTest() throws UnirestException 
    {
        this.yandexAnswer = new YandexWeather("c269590a-35b9-41be-ad70-ae820f162bc0");
        //Москва "55.75396"  "37.620393"
        this.lat = "55.75396";
        this.lon = "37.620393";
        
        this.weatherResult = yandexAnswer.giveMeWeatherForTwoDays(this.lat, this.lon);
    }
    
    @Test
    public void testLAT() throws Exception {
        ArrayList<String> way = new ArrayList();
        way.add("info");
        way.add("lat");
        
        assertEquals(this.lat, this.giveMeValue(way, this.weatherResult));
        //fail("testLAT fail");
    }
    
    @Test
    public void testLON() throws Exception {
        ArrayList<String> way = new ArrayList();
        way.add("info");
        way.add("lon");
        
        assertEquals(this.lon, this.giveMeValue(way, this.weatherResult));
        //fail("testLON fail");
    }
    
    @Test
    public void testOFFSET() throws Exception {
        ArrayList<String> way = new ArrayList();
        way.add("info");
        way.add("tzinfo");
        way.add("offset");
        
        assertEquals("10800", this.giveMeValue(way, this.weatherResult));
        //fail("testOFFSET fail");
    }
    
    @Test
    public void testNAME() throws Exception {
        ArrayList<String> way = new ArrayList();
        way.add("info");
        way.add("tzinfo");
        way.add("name");
        
        assertEquals("Europe/Moscow", this.giveMeValue(way, this.weatherResult));
        //fail("testNAME fail");
    }
    
    @Test
    public void testABBR() throws Exception {
        ArrayList<String> way = new ArrayList();
        way.add("info");
        way.add("tzinfo");
        way.add("abbr");
        
        assertEquals("MSK", this.giveMeValue(way, this.weatherResult));
        //fail("testABBR fail");
    }
    
    @Test
    public void testURL() throws Exception {
        ArrayList<String> way = new ArrayList();
        way.add("info");
        way.add("url");
        
        assertEquals("https://yandex.ru/pogoda/?lat=" + this.lat + "&lon=" + this.lon, this.giveMeValue(way, this.weatherResult));
        //fail("testURL fail");
    }
    
    @Test
    public void testForecastsDateCount() throws Exception {
        JSONObject jo = new JSONObject(this.weatherResult);
        JSONArray ja = (JSONArray) jo.get("forecasts");
        
        assertEquals("2", Integer.toString(ja.length()));
        //fail("testCountDate fail");
    }
    
    @Test
    public void testSEASON() throws Exception {
        ArrayList<String> way = new ArrayList();
        way.add("fact");
        way.add("season");
        
        assertEquals(this.giveMySeason(), this.giveMeValue(way, this.weatherResult));
        //fail("testSEASON fail");
    }
    
    @Test
    public void testMOON() throws Exception {
        JSONObject jo = new JSONObject(this.weatherResult);
        JSONArray ja = (JSONArray) jo.get("forecasts");
        
        assertEquals(true, this.checkMoon(ja.get(1).toString()));
        //fail("testMOON fail");
    }
    
    
    private String giveMeValue(ArrayList<String> arr, String json)
    {
        JSONObject jo = new JSONObject(json);
        for(int i = 0; i < arr.size() - 1; i++)
        {
            try{
            jo = (JSONObject) jo.get(arr.get(i));
            }
            catch(Exception e){}
        }
        
        return jo.get(arr.get(arr.size() - 1)).toString();
    }
    

    private String giveMySeason()
    {
        GregorianCalendar  date = new GregorianCalendar();
        int month = date.get(Calendar.MONTH);
        if(month == 11 && month <= 1)
            return "winter";
        if(month >= 2 && month <= 4)
            return "spring";
        if(month >= 5 && month <= 7)
            return "summer";
        if(month >= 8 && month <= 10)
            return "autumn";        
        return "I have a bad news =( ";
    }
    
    private boolean checkMoon(String json)
    {
        JSONObject jo = new JSONObject(json);
        int code = jo.getInt("moon_code");
        String text = jo.getString("moon_text");
        
        if(code == 0)
        {
            if(text.equals("full-moon"))
                return true;
            else
                return false;
        }
        if( (code >= 1 && code <= 3) || (code >= 5 && code <= 7) )
        {
            if(text.equals("decreasing-moon"))
                return true;
            else
                return false;
        }
        if(code == 4)
        {
            if(text.equals("last-quarter"))
                return true;
            else
                return false;
        }
        if(code == 8)
        {
            if(text.equals("new-moon"))
                return true;
            else
                return false;
        }
        if( (code >= 9 && code <= 11) || (code >= 13 && code <= 15) )
        {
            if(text.equals("growing-moon"))
                return true;
            else
                return false;
        }
        if(code == 12)
        {
            if(text.equals("first-quarter"))
                return true;
            else
                return false;
        }
        return false;
    }
}
