package Yandex_Weather;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class YandexWeather 
{
    private String yandexApiKey;
    
    public YandexWeather(String apiKey)
    {
        this.yandexApiKey = apiKey;
    }
    
    public String giveMeWeatherForTwoDays(String lat, String lon) throws UnirestException
    {
        try{
        HttpResponse<JsonNode> jsonResponse = 
                Unirest.get("https://api.weather.yandex.ru/v1/forecast?" + "lat=" + lat + "&lon=" + lon + "&limit=2&hours=false").header("X-Yandex-API-Key", this.yandexApiKey).asJson();
        return jsonResponse.getBody().toString();
        }
        catch(UnirestException ue){System.out.println("giveMeWeatherForTwoDays : "); ue.printStackTrace(); return null;}
    }
}
